
export class Funcionario {
	
	public email: string;
	public nome: string;
	public sobreNome: string;
	public idade: number;
}