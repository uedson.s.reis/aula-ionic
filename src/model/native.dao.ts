import { Storage } from '@ionic/storage';
import { Funcionario } from './funcionario';
import { Injectable } from '@angular/core';

@Injectable()
export class DataAccessObject {

    private static readonly CHAVE: string = 'funcionarios';

    constructor(private storage: Storage) {}

    public guardar(funcionarios: Array<Funcionario>): void {
        this.storage.set(DataAccessObject.CHAVE, funcionarios)
        .then(
            data => {},
            error => alert("Deu erro: "+ error)
        );
    }

    public async recuperar(): Promise<Array<Funcionario>> {
        return (await this.storage.get(DataAccessObject.CHAVE));
    }
}