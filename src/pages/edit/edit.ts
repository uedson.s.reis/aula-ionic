import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Http } from '@angular/http';
import "rxjs/add/operator/map";
import { MyApp } from '../../app/app.component';

import { Funcionario } from '../../model/funcionario';

/**
 * Generated class for the EditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-edit',
  templateUrl: 'edit.html',
})
export class EditPage {

  funcionario: Funcionario;

  constructor(private navCtrl: NavController, param: NavParams, private http: Http) {

    if (param.data.funcionario) {
      this.funcionario = param.data.funcionario;
    } else {
      this.funcionario = new Funcionario();
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditPage');
  }

  cancelar(): void {
    this.navCtrl.pop();
  }

  salvar(): void {
    
    this.http.post(MyApp.SERVICE_URL+"contratar", this.funcionario).map(res => res.json()).subscribe(
      data => {
        this.navCtrl.pop();
      }, error => {
        alert("Erro ao salvar: "+ error);
      });
  }

}
