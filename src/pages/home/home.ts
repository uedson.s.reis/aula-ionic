import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Http } from '@angular/http';
import "rxjs/add/operator/map";

import { MyApp } from '../../app/app.component';
import { EditPage } from '../edit/edit';

import { Funcionario } from '../../model/funcionario';
import { DataAccessObject } from '../../model/native.dao';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  funcionarios: Array<Funcionario>;

  constructor(private navCtrl: NavController, http: Http, dao: DataAccessObject) {
    let msgError = "Verifique sua conexão com a internet!";
    
    http.get(MyApp.SERVICE_URL+"funcionarios").map(res => res.json())
    .subscribe(data => {
      this.funcionarios = data;
      dao.guardar(data);
    
    }, error => {
      dao.recuperar().then(
        data => {
          alert(data);
          
          if (data) this.funcionarios = data;
          else alert(msgError);

        }, error => alert("Error: "+ error));
    });
  }

  editar(funcionario: Funcionario) {
    this.navCtrl.push(EditPage, { funcionario: funcionario });
  }

  criarNovo() {
    this.navCtrl.push(EditPage);
  }
}
