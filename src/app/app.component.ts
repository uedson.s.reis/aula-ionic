import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  public static readonly SERVICE_URL: string = "https://us-central1-aula-f607b.cloudfunctions.net/";
  // public static readonly SERVICE_URL: string = "http://localhost:8080/";

  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then((texto) => {

      statusBar.styleDefault();
      splashScreen.hide();
      
    });
  }
}

